package at.campus02.nowa.prg3.multithreading.thread;

import java.util.ArrayList;
import java.util.List;

public class TestApp {

	public static void main(String[] args) {
		List<Thread> l = new ArrayList<Thread>();
		
		for (int i = 0; i < 50000; i++) {
			Thread t = new Thread(); 
			l.add(t);
			t.start();
		}
		
		System.out.println("Fertig");
	}

}
