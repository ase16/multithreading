package at.campus02.nowa.prg3.multithreading.thread;

public class TestThread extends Thread {

	@Override
	public void run() {
		while (true) {
			System.out.println("Thread bei der Arbeit: " + Thread.currentThread().getId());
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
