package at.campus02.nowa.prg3.multithreading.runnable;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

public class CharacterMain {

	public static void main(String[] args) {
		HashMap<Thread, CharacterRunnable> threads = new HashMap<Thread, CharacterRunnable>();
		
		for (char c : "abcde".toCharArray()) {
			CharacterRunnable r = new CharacterRunnable(c);
			Thread t = new Thread(r);
			threads.put(t, r);
			t.start();
		}
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (Entry<Thread, CharacterRunnable> e : threads.entrySet()) {
			e.getValue().halt();
		}
	}

}
