package at.campus02.nowa.prg3.multithreading.runnable;

public class CharacterRunnable implements Runnable {
	private char buchstabe;
	private boolean stopped = false;
	
	public CharacterRunnable(char buchstabe) {
		this.buchstabe = buchstabe;
	}
	
	public void halt() {
		stopped = true;
	}

	public void run() {
		while (!stopped) {
			System.out.println(buchstabe);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
